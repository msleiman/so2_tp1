/** @file codigos.c

    @brief contiene las cadenas de caracteres usadas como herramienta de control
    y manejo de errores en la comunicacion entre cliente y servidor.

 */


char *FIRM_TRANS_FINISH = "FIRM_TRANS_FINISH_5757575";
char *UDP_OPEN_OK = "SOCKET_UDP_OPEN_5757575";
char *FIN_CONECTION = "FIN_CONECTION_5757575";
char *FILE_NOT_FOUND = "FILE_NOT_FOUND_5757575";
char *FILE_FOUND = "FILE_FOUND_5757575";
char *MS_FAIL_AUTH ="MS_FAIL_AUTH_5757575";

//comandos
char *START_SCANNING = "start scanning";
char *UPDATE_FIRMWAREBIN = "update firmware.bin";
char *OBTENER_TELEMETRIA = "obtener telemetria";

#define check_socket_read(n) \
{ \
	if (n < 0) { \
            perror( "Problema en lectura de socket" ); \
		    exit(1); \
	} \
}

#define check_socket_write(n) \
{ \
	if (n < 0) { \
            perror( "Problema en escritura de socket" ); \
		    exit(1); \
	} \
}

