var searchData=
[
  ['readme',['README',['../md__r_e_a_d_m_e.html',1,'']]],
  ['readme_2emd',['README.md',['../_r_e_a_d_m_e_8md.html',1,'']]],
  ['recibir_5fbin_5ftcp',['recibir_BIN_TCP',['../cliente__includes_8h.html#aa5d468e0a22ea4ab71e5da0c13d98ba7',1,'recibir_BIN_TCP(int):&#160;cliente.c'],['../cliente_8c.html#abdfa45a133687b2c04ce5bd9e6c4d231',1,'recibir_BIN_TCP(int sockfd):&#160;cliente.c']]],
  ['recibir_5ftelemetria_5fudp',['recibir_telemetria_UDP',['../server__includes_8h.html#a73263021b9ac687f470a277b58e3eece',1,'recibir_telemetria_UDP(int, struct sockaddr_un *):&#160;servidor.c'],['../servidor_8c.html#a46d657d3be9efaa20df1b0457e1b23f1',1,'recibir_telemetria_UDP(int newsockfd_UDP, struct sockaddr_un *srv_addr_udp):&#160;servidor.c']]],
  ['recieve_5fscanning',['recieve_scanning',['../server__includes_8h.html#a78fd72621be65e2c495163b03231a550',1,'recieve_scanning(int, time_t):&#160;servidor.c'],['../servidor_8c.html#a20d20ea99a926ab529850c47a5b659cd',1,'recieve_scanning(int newsockfd, time_t start_time_scanning):&#160;servidor.c']]]
];
