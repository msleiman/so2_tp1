/**
 @file servidor.c
 @brief Ejecuta el lado servidor (una estacion terrena) en un host .
 Para correrlo se debe ejecutar desde la terminal de la forma ./serivdor direccionIPv4 puerto.
 La contraseña del servidor es "bigmac". Acepta multiples conexiones simultaneas.
 Una vez establecida la conexion con el cliente permite ennviarle los siguientes comandos:
 - update firmware.bin: enviar un archivo binario al cliente (satelite), una vez recibido se reinicia
 el satelite y vuelve a reintentar la conexion con el servidor (el firmware es un archivo binario con
 el numero de actualizacion).
 - start scanning: ordena al satelite iniciar el escaneo de toda la cara de la Tierra y luego
 recibe el escaneo en forma de paquetes de tamaño igual al MSS para evitar fragmentacion del protocolo IP.
 Se determina el tiempo que transcurre entre que se envia el comando y la recepcion del ultimo paquete.
 - obtener telemetrı́a:  la estacion terrena recibce del satélite la siguiente información: – Id del satélite
 – Uptime del satélite – Versión del software – Consumo de memoria y CPU

	@author Sleiman, Mohamad
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>

#include "../includes/server_includes.h"
#include "../includes/codigos.h"

/**
@brief Funcion main del cliente.
Recibe direccion y puerto del servidor como parametros en arvg[], luego solicita la autenticacion
que permite 3 reintentos para luego cerrar el programa. Una vez completada la autenticacion
abre el puerto TCP que queda a la espera de conexiones del cliente (satelite):
acepta hasta 5 conexiones de clientes simulataneas (aunque se utilizaran solo una por el momento).
Una vez establecida la conexion permite el envio de comando al satelite:
  - update firmware.bin: enviar un archivo binario al cliente (satelite), una vez recibido se reinicia
 el satelite y vuelve a reintentar la conexion con el servidor (el firmware es un archivo binario con
 el numero de actualizacion).
 - start scanning: ordena al satelite iniciar el escaneo de toda la cara de la Tierra y luego
 recibe el escaneo en forma de paquetes de tamaño igual al MSS para evitar fragmentacion del protocolo IP.
 Se determina el tiempo que transcurre entre que se envia el comando y la recepcion del ultimo paquete.
 - obtener telemetrı́a:  la estacion terrena recibce del satélite la siguiente información: Id del satélite,
 Uptime del satélite, Versión del software, Consumo de memoria y CPU.
 El envio de la cadena "exit" cierra la conexion con el cliente (satelite).
 El envio y recepcion de telemetria se realiza sobre un socket Datagram.
*/



#define FIRMWARE_BIN "./recursos_estacion/firmware.bin"
#define IMAGEN_SCANNING "./recursos_estacion/imagen.jpg"


int main(int argc, char *argv[]) {


	//////// AUTENTICACION ////////

	int autenticacion_ok = autenticacion();
	if (autenticacion_ok == 0) exit(0); //nos vamos del programa

	vaciar_buffer_teclado();

	//////// CREACION DE SOCKET STREAM ////////

	int sockfd, newsockfd, puerto;
	pid_t pid;
	socklen_t clilen;
	char buffer[TAM];
	char *direccionIPv4;
	struct sockaddr_in serv_addr, cli_addr;
	int n;

	if (argc < 3) { //comprobamos funcionamiento en llamada
		fprintf( stderr, "Uso: %s ipv4 puerto\n", argv[0]);
		exit(1);
	}


	/*Creacion de socket
	* int socket( int domain, int type, int protocol );
	* domain: tipo de direccion almacenada (AF_INET dominio de internet)
	* tipo: indica tipo de socket (sock_stream secuencia de caracateres)
	* protocolo: 0, significa que el protocolo es un valor por defecto
	* determinado por domain y type (que en este caso da un socket TCP).
	*
	* Devuelve un file descriptor con el numero mas bajo que no este
	* abierto para el proceso
	*/
	sockfd = socket( AF_INET, SOCK_STREAM, 0); //socket le dice al SO que queremos un descriptor de archivo de socket.
	if (sockfd < 0) {
		perror(" ERROR en la apertura de socket ");
		exit(1);
	}

	direccionIPv4 = argv[1]; //asignamos una ip (De la interfaz interfaz ifconfig  --> 192.168.... o localhost)
	puerto = atoi(argv[2]); //convertimos el puerto a un entero

	//limpieza de estructura
	memset((char *) &serv_addr, 0, sizeof(serv_addr));

	//completamos la estructura serv_addr
	serv_addr.sin_family = AF_INET;
	inet_aton(direccionIPv4, &serv_addr.sin_addr); //funcion que transforma un direccion Ipv4 en binario.
	serv_addr.sin_port = htons(puerto); //htons convierte el INT UNSIGNED del orden de bytes del host a orden de bytes de la red (que es BIG ENDIAN)

	/*
	* hacemos el bind, primero casteamos la struc sockaddr_in a sockaddr
	 * (ligamos puerto y direccion IP a un socket)
	*/

	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) { //PREGUNTAR
		perror("ligadura");
		exit(1);
	}

	printf("Proceso: %d - socket disponible --->  IPv4 %s - puerto %d \n",
		   getpid(), direccionIPv4, ntohs(serv_addr.sin_port));

	/*
	* listen pone en escucha un socket, que acepta MAX_CONEXIONES conexiones simultaneas
	* accept se encarga de aceptar una conexion entrante en un socket y crea
	* otro socket inmediatamente con las mismas propiedades,
	* el socket original vuelve al estado de escucha.
	*/

	listen(sockfd, MAX_CONEXIONES); //sockdf es un socket pasivo para aceptar solicitudes de conexiones
	clilen = sizeof(cli_addr);

	while (1) {

		/*
         * accept() extrae la primer solicitud de conexion en la cola de conexiones pendientes
         * para el socket de escucha (sockfd), crea un nuevo socket y devuelve el file descriptor.
         * Este nuevo socket esta formado por las direcciones y puertos locales y remotos.
         */

		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen); //
		if (newsockfd < 0) {
			perror("accept");
			exit(1);
		}


		pid = fork(); //Rutina para el cliente
		if (pid < 0) {
			perror("fork");
			exit(1);
		}

		if (pid == 0) {  // Proceso hijo, cerramos el file descritpor sockfd del hijo (no en el padre)

			close(sockfd);

			n = read( newsockfd, buffer, TAM-1 );
			check_socket_read(newsockfd);



			while ( 1 ) {

				memset(buffer, '\0', TAM);

				printf("enviar a estacion >> ");
				fgets(buffer, TAM - 1, stdin);

				n = write(newsockfd, buffer, sizeof(buffer));
				check_socket_write(n);


				//leo respuesta de la estacion
				memset( buffer, '\0', TAM );

				n = read( newsockfd, buffer, TAM-1 );
				check_socket_read(n);


				if( !strcmp(buffer, FIN_CONECTION ) ) {

					printf("PROCESO %d. Satelite desconectado. \n", getpid());
					exit(1);

				} else if ( strcmp(OBTENER_TELEMETRIA, buffer) == 0 ){ //cliente me confirma apertura de puerto

					printf("PROCESO %d. Recepcion de telemetria. \n", getpid());

					//Abro puerto UDP
					struct sockaddr_in srv_addr_udp;
					int newsock_udp = open_UDP_server(newsockfd, &srv_addr_udp);


					if( newsock_udp == 0){

						printf("PROCESO %d. Problemas en la apertura del socket UDP. \n", getpid());

						n = write(newsockfd, MS_FAIL_AUTH, strlen(MS_FAIL_AUTH));
						check_socket_write(n);

					} else {

						//Confirmo apertura de mi puerto UDP sin problemas
						n = write(newsockfd, UDP_OPEN_OK, strlen(UDP_OPEN_OK));
						check_socket_write(n);

						//Recibo descarga UDP
						recibir_telemetria_UDP(newsock_udp, &srv_addr_udp);
						close(newsock_udp);

					}


				} else if ( strcmp( buffer, UPDATE_FIRMWAREBIN) == 0 ){

					printf("PROCESO %d. Preparando envio del firmware. \n", getpid());
					enviar_BIN_TCP(newsockfd);
					printf("PROCESO %d. Finalizado envio del firmware. \n", getpid());


					//recibo mensaje de fin para cerrar la conexion

					memset( buffer, '\0', TAM );

					n = read( newsockfd, buffer, TAM-1 );
					check_socket_read(n);


					if ( strcmp(FIN_CONECTION, buffer) == 0 ) {

						printf("PROCESO %d. Satelite reiniciadose. \n", getpid());
						printf("PROCESO %d. Conexion cerrada. \n", getpid());
						exit(1);


					}

				} else if ( strcmp(buffer, START_SCANNING) == 0 ){

					//para medir el tiempo en que recibo el datagrama
					time_t start_time_scanning;
					time(&start_time_scanning);

					printf("PROCESO %d. Activo rutina de escaneo en el satelite. \n", getpid());
					recieve_scanning(newsockfd, start_time_scanning);
					printf("PROCESO %d. Finalizado escaneo del satelite. \n", getpid());

				} else {

					printf("Recibi del satelite: %s", buffer);
				}

			}

		} else {
			printf("SERVIDOR: Nuevo cliente, que atiende el proceso hijo: %d\n", pid);
			close(newsockfd); //proceso padre cerramos el file descriptor newsockfd en el padre
		}
	}
}


/**
@brief Recibe el escaneo por parte del satelite en paquetes cuyo tamaño no sobrepasa el
MSS (maximun segment size) = MTU (EJEMPLO 1500 bytes en ethernet) - 20 bytes header TCP - 20 bytes IP Header.
(tambien es posible que tengamos encapsulacion MPLS que agrega 12 bytes adicionales reduciendo el MSS) .
Por lo tanto se ajusta el MSS a 1448 bytes.
 Se reciben la cantidad de paquetes totales que enviara el satelite con tamaño es igual al
 MSS, de esta forma el servidor alloca memoria y procede a su recepcion.
 Una vez finalizado el envio de paquetes se calcula el tiempo transcurrido enntre que se envia el comando
 y la llegada del ultimo paquete y se reconstruye la imagen que se guarda en la carpeta:
 ./recursos_estacion/imagen.jpg
@param newsockfd descriptor de socket TCP.
@param start_time_scanning tiempo en que se envia el comando "start scanning" al cliente.
@return 1 si la transferencia y armado se realiza correctamente, 0 caso contrario.
*/

int recieve_scanning(int newsockfd, time_t start_time_scanning ){

	int n = 0;
	char buffer[TAM];
	long n_matriz = 0;
	int fd;
	time_t stop_time_scanning; // para medir el tiempo que tardo en recibir el ultimo paqute


	memset(buffer, '\0', TAM);

	n = read(newsockfd, buffer, TAM);
	check_socket_read(n);

	printf("n: %d \n", n);

	if (strcmp(FILE_NOT_FOUND, buffer) == 0) { //el servidor no puede realizar el escaneo

		printf("Problemas del satelite para realizar el escaneo. \n");
		return 0;
	}

	//recibo cantidad de paquetes para allocar la matriz en caso de no haber recibido FILE_N0T_FOUND

	n_matriz = atoi(buffer);

	printf("PROCESO %d. Numero de paquetes %ld \n", getpid(), n_matriz);

	// printf (" N_MATRIZ: %ld \n", n_matriz);

	char **partes_escaneo = malloc( n_matriz * sizeof(char*));
	for(long i = 0; i < n_matriz; i=i+1) partes_escaneo[i] = malloc(MSS * sizeof(char)); //sizeof(char) es 1

	//recibimos paquetes
	printf("PROCESO %d. Recibiendo escaneo. \n", getpid());

	//  sleep(10);

	long i = 0;
	n = 0;
	while( (n = read(newsockfd, partes_escaneo[i], MSS) ) > 0 ) { //vamos leyendo archivo

		//printf("%s", partes_escaneo[i]);
		//printf("n: %d \n", n);

		i =i +1 ;
		if(i >= n_matriz) break;
		printf("\rDescargando %ld de %ld paquetes. ",  i+1, n_matriz); //\r para escribir sobre la linea actual

	}


	printf("\n");

	//calculo tiempo de diferencia
	time(&stop_time_scanning);
	double t_diff = difftime (stop_time_scanning, start_time_scanning);

	printf("PROCESO %d. El escaneo tardo: %d segundos. \n", getpid(), (int)t_diff);//casteamos a int para sacar la coma
	printf("PROCESO %d. Rearmando escaneo. \n", getpid());


	//creamos el archivo imagen.jpg en carpeta recursos_estacion
	fd = open(IMAGEN_SCANNING, O_RDWR | O_CREAT, 0666); //creamos un archivo firmware bin si no existe

	//reconstruimos la imagen
	for(long i = 0; i < n_matriz; i=i+1){

		n = write( fd, partes_escaneo[i], MSS );

	}

	close (fd);

	//liberamos memoria
	for(long i = 0; i < n_matriz; i=i+1) free (partes_escaneo[i]);
	free(partes_escaneo);


	return 1;

}

/**
@brief Envia un archivo binario por el mismo socket TPC. Abre el archivo en caso
 de haber fallas envia un mensaje de error al satelite. Envia al satelite el tamaño
 del archivo total para que el satelite pueda allocar la memoria correspondiente.
 Luego comienza a leer el archivo de TAM bytes en TAM bytes y envia esos TAM bytes
 leidos a traves del socket TCP.
@param newsockfd descriptor de socket TCP.
@return 1 si la transferencia se realiza correctamente, 0 caso contrario.
*/

int enviar_BIN_TCP(int newsockfd){


	int fd;
	int	n;
	char buffer[TAM];
	struct stat file_stat; //para obtener informacion del archivo
	int bytes_a_enviar = 0;

	memset(buffer, '\0', TAM);

	fd = open(FIRMWARE_BIN, O_RDONLY);


	if( (fd == -1) || (fstat(fd, &file_stat) < 0)   ) { //error abriendo el archivo o leyendo datos

		n = write( newsockfd, FILE_NOT_FOUND, strlen(FILE_NOT_FOUND) );
		check_socket_write(n);

		return 0;

	} else {    //envio tamaño del archivo

		sprintf(buffer, "%ld", file_stat.st_size);

		printf("PROCESO %d. Tamaño del firmware a enviar: %ld bytes. \n", getpid(), file_stat.st_size);

		n = write(newsockfd, buffer, sizeof(buffer));
		check_socket_write(n);

	}


	printf("PROCESO %d. Enviando archivo. \n", getpid());


	//leo el archivo en TAM bytes.
	while( ( bytes_a_enviar = read(fd, buffer, TAM) ) > 0 ) { //vamos leyendo archivo y enviando

		//mando por el socket
		n = write(newsockfd, buffer, bytes_a_enviar); //envio los bytes justos
		//n = write(newsockfd, buffer, sizeof(buffer));
		check_socket_write(n);

	}

//	n = write(newsockfd, FIRM_TRANS_FINISH, strlen(FIRM_TRANS_FINISH));
//	check_socket_write(n);



	close (fd);
	return 1;

}


/**
@brief Crea un socket UDP (con la estructura de un servidor UDP) con la misma direccion y puerto que el socket TCP.
Mediante la funcion getsockname extrae la informacion asociada del socket TPC necesaria realizar el bind()
con el nuevo socket UDP, luego realiza el bind entre el nuevo socket UDP y la estructura llenada por la
funcion getsockname.
@param sockdf_tcp descriptor de socket TCP.
@param sockaddr_in *cli_addr_udp estructura que se carga con la informacion asociada
al socket TPC.
@return nuevo descriptor de socket UDP o 0 en caso de falla en la creacion.
*/

int open_UDP_server(int sockfd_tcp, struct sockaddr_in *srv_addr_udp)
{
	int sockfd_udp;

	/*getsocketname recupera los datos de la struct sockaddr_in ligada al server*/
	struct sockaddr_in srv_addr_tcp; //creamos la estructura para almacenar los datos extraidos de TCP
	socklen_t length = sizeof(struct sockaddr_in);
	getsockname(sockfd_tcp, (struct sockaddr *) &srv_addr_tcp, &length);

	char *ip_addr_server = inet_ntoa( srv_addr_tcp.sin_addr);
	int puerto = ntohs(srv_addr_tcp.sin_port);

	/*
    printf("IP_SERVER %s\n" ,ip_addr_server ) ;
    printf("puerto %d\n", puerto);
    */

	sockfd_udp = socket( AF_INET, SOCK_DGRAM, 0 );
	if (sockfd_udp < 0) {
		perror("ERROR en apertura de socket");
		return 0;
	}

	//ya es una direccion le sacamos los & y agreagamos *
	memset( srv_addr_udp, 0, sizeof(*srv_addr_udp) );
	srv_addr_udp->sin_family = AF_INET;
	inet_aton(ip_addr_server, &srv_addr_udp->sin_addr);
	srv_addr_udp->sin_port = htons( puerto );
	memset( (srv_addr_udp->sin_zero), '\0', 8 );

	if( bind( sockfd_udp, (struct sockaddr *) srv_addr_udp, sizeof(*srv_addr_udp) ) < 0 ) {
		perror( "ERROR en binding" );
		return 0;
	}

	printf( "Socket UDP disponible en puerto: %d\n", ntohs(srv_addr_udp->sin_port) );

	return sockfd_udp;
}


/**
@brief Recibe la telemetria del satelite mediante el socket UDP. A traves de la funcion
recvfrom.
@param sockdf_tcp descriptor de socket TCP.
@param sockaddr_in *cli_addr_udp que contiene la informacion asociada al socket UDP.
@return nuevo descriptor de socket UDP o 0 en caso de falla en la creacion.
*/

int recibir_telemetria_UDP(int newsockfd_UDP, struct sockaddr_in *srv_addr_udp){

	char buffer[ TAM ];
	memset( buffer, 0, TAM );
	socklen_t tam_direccion = sizeof( struct sockaddr );

	recvfrom( newsockfd_UDP, buffer, TAM, 0, (struct sockaddr *)srv_addr_udp, &tam_direccion );

	printf("Recibo telemetria: \n %s", buffer);

	return 1;


}


/**
@brief Funcion que realiza la autenticacion del server.
Si la cadena de caracteres es correcta se retorna con un 1.
En caso contrario la operacion se reitera una cantidad de veces determinada por
una variable de control (3 reintentos) hasta se ingrese correctamente la contraseña
o se acabe la cantidad de reintentos.
@return 1 autenticacion exitosa, 0 caso contrario.
*/

int autenticacion() {

	char buffer[TAM];
	int control = 1;
	int exito = 2;
	int reintentos = 3;


	while(1){

		memset( buffer, '\0', TAM );

		printf("Ingrese su contraseña: " );
		scanf("%s", buffer);
		buffer[strlen(buffer)] = '\0'; //Indicamos la terminacion del string.

		if (strcmp("bigmac", buffer) == 0) {

			printf(	"Autenticacion exitosa. \n");
			exito = 1;

		} else if(control >= reintentos){

			printf(	"Autenticacion fallida. \n");
			exito = 0;

		} else {

			control = control + 1;
			printf("Volver a intentar. \n" ); //aca recibe usuario

		}

		if(exito == 1 || exito == 0) break;


	}

	return exito;

}


/**
@brief Funcion que limpia el buffer del teclado.
Fuente: https://poesiabinaria.net/2009/05/buffer-de-teclado-en-linux/
@param
*/


void vaciar_buffer_teclado()
{
    int fdflags;



    //F_GETFL -> devuelve el modo de acceso y el status del descriptor.
    fdflags = fcntl(STDIN_FILENO, F_GETFL, 0);

    //F_SETFL -> pone la bandera de status al valor especificado por el tercer
    //argumento, configuramos el descriptor para el acceso no bloqueante.
    fcntl(STDIN_FILENO, F_SETFL, fdflags | O_NONBLOCK);
    //en un caso bloqueante, getchar() no devuelve nada hasta que se presione enter
    while (getchar()!=EOF);
    //restauramos el status del STDIN
    fcntl(STDIN_FILENO, F_SETFL, fdflags);
}
