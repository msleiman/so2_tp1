Sistemas Operativos 2 - Trabajo Practico N°1 2019
================================================================================

Sleiman, Mohamad Ihab. 
Matricula: 42828207.

--------------------------------------------------------------------------------

  Leer archivo makefile y seleccionar modo de compilacion adecuado, para raspberry
  o entorno linux x64 bits.

--------------------------------------------------------------------------------

Instrucciones para sockets Internet:
    
    1) Mediante terminal ir a la carpeta del proyecto.
    2) Ejecutar make.
    3) Asegurarse que servidor y cliente se ejecuten en dipositivos conectados en la misma red.
    4.1) Ejecutar servidor que se encuentra en carpeta bin:
        
        servidor: ./bin/servidor ip_servidor puerto
        ejemplo ./bin/servidor 192.168.0.10 10300 
        
        
    4.2) Ejecutar cliente que se encuetnra en carpeta bin (el cliente se conectara por defecto 
	 a la direccion 192.168.0.10 puerto 10300, para cambiar modificar las variables corres-
	 pondientes en el archivo cliente.c de la carpeta scr luego omitir este paso y volver
	 al paso 2):
        
         cliente: ./bin/cliente
    
        
--------------------------------------------------------------------------------

Instrucciones para sockets UNIX:
    
    1) Mediante terminal ir a la carpeta del proyecto.
    2) Ejecutar make.
    3) Asegurarse que servidor y cliente se ejecuten en dipositivos conectados en la misma red.
    4.1) Ejecutar servidor que se encuentra en carpeta bin:
        
        servidor: ./bin/servidor path
        ejemplo ./bin/servidor server_estacion
        
        
    4.2) Ejecutar cliente que se encuetnra en carpeta bin (el cliente se conectara por defecto 
	 al path server_estacion para cambiar modificar las variables correspondientes en el 
	 archivo cliente.c de la carpeta scr luego omitir este paso y volver al paso 2):
        
         cliente: ./bin/cliente
    
        
--------------------------------------------------------------------------------

Contraseña del servidor: 
    
    bigmac

--------------------------------------------------------------------------------

 Documentacion del codigo:
    
    sockets_*/doc_doxygen/html/index.html

