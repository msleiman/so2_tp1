var codigos_8h =
[
    [ "check_socket_read", "codigos_8h.html#ac37d6ea247acbc9c23ebfb1a0e13d138", null ],
    [ "check_socket_write", "codigos_8h.html#a80c1e007b4251dffc52435606e4e7ebf", null ],
    [ "FILE_FOUND", "codigos_8h.html#ac1f80c6cc38551c4ae2ed5d45aaba405", null ],
    [ "FILE_NOT_FOUND", "codigos_8h.html#a1c4da909cab39fba1661af7d902c7d09", null ],
    [ "FIN_CONECTION", "codigos_8h.html#a2fb1c4680361339cbd1ba7b5e1f7b086", null ],
    [ "FIRM_TRANS_FINISH", "codigos_8h.html#a76f75e433310906e6e6e225d0df50b99", null ],
    [ "MS_FAIL_AUTH", "codigos_8h.html#a835b1658854cc4f88c8ff7f46cb4c887", null ],
    [ "OBTENER_TELEMETRIA", "codigos_8h.html#a0465fd0da0792358a455f9425c6e4576", null ],
    [ "START_SCANNING", "codigos_8h.html#aded776189955bcf28ec60b216038abe5", null ],
    [ "UDP_OPEN_FAIL", "codigos_8h.html#ad7bd00a7a68986f17000c447c6e1860a", null ],
    [ "UDP_OPEN_OK", "codigos_8h.html#a58560f0022347829e8b78f28ed5cc207", null ],
    [ "UPDATE_FIRMWAREBIN", "codigos_8h.html#ab9b7281464c7566b1e1a7b3bcc44c98b", null ]
];