/** @file cliente_includes.c

    @brief contiene la declaracion de funcion y variables utlizadas en el cliente.

 */

#define TAM 1448
#define MSS 1448 // 1500 MTU - 20 BYTE TCP HEADER - 20 BYTES IP HEADER = 1460

int autenticacion(int , char* );
int empieza_con(char* , char* );
int open_UDP_client();
int enviar_telemetria_UDP( int,struct sockaddr_in *);
int recibir_BIN_TCP(int);
void write_file(char const* path, char* buffer);
int scanning(int);
double loadCPU(void);


