/**
*	@file cliente.c

* @brief Ejecuta el lado cliente (un satelite) en un sistema embebido (RASPBERRY PI) .
* Para correrlo se debe ejecutar desde la terminal de la forma ./cliente intentara conectarse
* a una direccion ip y puerto especifico.
* Una vez establecida la conexion con el server puede recibir los siguientes comandos:
* - update firmware.bin: recibe un archivo binario de la estacion terrena (server), una vez recibido se reinicia
* el satelite y vuelve a reintentar la conexion con el servidor (el firmware es un archivo binario con
* el numero de actualizacion).
* - start scanning:  inicia el escaneo de toda la cara de la Tierra lo particiona paquetes igual al MSS
+ (para evitar la fragmentacion) y luego lo envia al server.
* - obtener telemetrı́a:  envia a la estacion terrena la siguiente información: Id del satélite,
* Uptime del satélite, Versión del software, Consumo de memoria y CPU.

	@author Sleiman, Mohamad
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <time.h>
#include <math.h>



#include "../includes/cliente_includes.h"
#include "../includes/codigos.h"

#define FIRMWARE_BIN "./recursos_satelite/firmware.bin"
#define IMAGEN_SCANNING "./recursos_satelite/imagen.jpg"


char *ID_SATELITE = "FCEFYNSAT";
char *version_software = "1";

//char *IP_estacion = "localhost";
char *IP_estacion = "192.168.0.10";
char *puerto_estacion = "10300";



/**
@brief Funcion main del cliente.
*Intenta la conexion con el server cuya direccion y puerto esta determianda por las variables
* IP_estacion (direccion ipv4) y puerto_estacion. En caso de fallar la conexion vuelve a reintarlo
* reitereadas veces.
* Una vez establecida la conexion con el server puede recibir los siguientes comandos:
* - update firmware.bin: recibe un archivo binario de la estacion terrena (server), una vez recibido se reinicia
* el satelite y vuelve a reintentar la conexion con el servidor (el firmware es un archivo binario con
* el numero de actualizacion).
* - start scanning:  inicia el escaneo de toda la cara de la Tierra lo particiona paquetes igual al MSS
+ (para evitar la fragmentacion) y luego lo envia al server.
* - obtener telemetrı́a:  envia a la estacion terrena la siguiente información: Id del satélite,
* Uptime del satélite, Versión del software, Consumo de memoria y CPU.
* El envio de la cadena "exit" cierra la conexion con el cliente (satelite).
* El envio y recepcion de telemetria se realiza sobre un socket Datagram.
*/



int main( int argc, char *argv[] ) {


    int sockfd, puerto, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char buffer[TAM];

    //printf( "Intentado conexion con la estacion - Usuario: %s - IPserver: %s - Puerto %s  \n", ID_SATELITE, IP_estacion, puerto_estacion );


    puerto = atoi( puerto_estacion );
    sockfd = socket( AF_INET, SOCK_STREAM, 0 );
    if ( sockfd < 0 ) {
        perror( "ERROR apertura de socket" );
        exit( 1 );
    }

    /*
    * gethostbyname devuelve una estructura del tipo hostent
    * para una direccion de host dada.
    * si le pasamos como parametro una direccion IPv4
    * copia el nombre en el campo h_name ( copia la direccion
    * como un char), h_addrtype en 2 para AF_INET,
    * h_length en 4 para IPv4 y
    * su estructura in_addr en el campo h_addr_list
    *
    * la estructura hostent sirve para asociar los datos
    * del cliente.
    */

    server = gethostbyname( IP_estacion );
    if (server == NULL) {
        fprintf( stderr,"Error, no existe el host\n" );
        exit( 1 );
    }


    memset( (char *) &serv_addr, '0', sizeof(serv_addr) );
    serv_addr.sin_family = AF_INET;

    //copiamos el contenido de la struct in_addr en la struct sockaddr_in del server
    bcopy( (char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length );
    serv_addr.sin_port = htons( puerto );

    /*
    * Funcion connect establece la conexion con el server
    * Si el socket no se ha vinculado a una dirección local,
    * connect () lo vinculará a una.
    */

    int con_status = -1;

    while (1){ //esperamos hasta lograr la conexion

        printf( "Intentado conexion con la estacion - Usuario: %s - IPserver: %s - Puerto %s  \n", ID_SATELITE, IP_estacion, puerto_estacion );

        con_status =  connect( sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr ) );
        // printf ("conecct: %d \n ", auxux );

        if ( con_status  >= 0 )
        {
            printf( "Conexion exitosa.  \n");
            break;
        }

        sleep(2); //si no logramos la conexion dormimos 10 segundos

    }


    //MENSAJE DE BIENVENIDA ACA PODRIA MANDAR NUMERO DE SATELITE
    memset( buffer, 0, TAM );
    memcpy(buffer, "Satelite conectado. \n", 23);

    n = write( sockfd, buffer, strlen(buffer) );
    check_socket_write(n);


    while(1) {

        memset( buffer, '\0', TAM );

        n = read( sockfd, buffer, TAM );
        check_socket_read(n);

        printf( "Recibí de la estacion: %s", buffer );

        buffer[strlen(buffer) - 1] = '\0'; //agregamos fin de archivo

        if( !strcmp( "exit", buffer ) ) {

            n = write( sockfd, FIN_CONECTION, strlen(FIN_CONECTION));
            check_socket_write(n);

            exit(1);
            break;

        } else if ( strcmp(buffer, OBTENER_TELEMETRIA ) == 0 ) {

            //Abrimos el socket UDP y la ligamos a la structura sockaddr

            int newsockfd_UDP = open_UDP_client();

            //confirmamos apertura del puerto UDP
            n = write(sockfd, OBTENER_TELEMETRIA, 18);
            check_socket_write(n);

            //Esperamos la confirmacion de la apertura de puerto UDP del server
            memset(buffer, '\0', TAM);

            n = read(sockfd, buffer, TAM);
            check_socket_read(n);

            if (strcmp(UDP_OPEN_OK, buffer) == 0) {
                enviar_telemetria_UDP(newsockfd_UDP, &serv_addr);
            } else {
                printf("Problemas para abrir el socket UDP en el server. \n");
            }

            close(newsockfd_UDP);


        } else if ( strcmp(buffer, UPDATE_FIRMWAREBIN) == 0 ){

            printf("Preparando recepcion del firmware. \n");
            recibir_BIN_TCP(sockfd);


        } else if ( strcmp(buffer, START_SCANNING) == 0 ){

            printf("Solicitud de escaneo. \n");
            scanning(sockfd);
            printf("Terminado el escaneo. \n");


        } else {

            n = write( sockfd, "Comando no identificado \n", 26 );
            check_socket_write(n);
        }


    }

    return 0;

}

/**
@brief Envia el escaneo  en paquetes cuyo tamaño no sobrepasa el
MSS (maximun segment size) = MTU (EJEMPLO 1500 bytes en ethernet) - 20 bytes header TCP - 20 bytes IP Header.
(tambien es posible que tengamos encapsulacion MPLS que agrega 12 bytes adicionales reduciendo el MSS) .
Por lo tanto se ajusta el MSS a 1448 bytes.
Se escanea la imagen imagen.jpg que esta en el directorio recursos_satelite.
Se calcula la cantidad de paquetes como el tamaño total sobre el MSS (tam_total/MSS) para allocar la memoria
correspondiente y luego particionar la imagen y guardarla en forma de paquetes de tamaño MSS en la memoria.
Se envia la cantidad de paquetes al servidor para que reserve la memoria correspondiente y luego se
procede al envio de cada uno de los paquetes.
@param newsockfd descriptor de socket TCP.
@return 1 si la transferencia y armado se realiza correctamente, 0 caso contrario.
*/


int scanning (int sockfd){

    int n = 0;
    long n_matriz = 0;
    char buffer[TAM];
    int fd;
    struct stat file_stat;
    long bytes_a_guardar;


    //confirmamos inicio de escaneo
    n = write(sockfd, START_SCANNING, strlen(START_SCANNING));
    check_socket_write(n);

    //abrimos archivo
    fd = open(IMAGEN_SCANNING, O_RDONLY);

    //aca pedir el mms en una funcion

    //abrimos imagen


    if( (fd == -1) || fstat(fd, &file_stat)   ) { //error abriendo el archivo o leyendo datos

        n = write( sockfd, FILE_NOT_FOUND, strlen(FILE_NOT_FOUND) );
        check_socket_write(n);

        return 0;

    } else {

        //proceso de division de la imagen, a la imagen la divido en matrices:
        //calculo el tamaño de la matriz nxm (m es el MSS)

        float aux =  (float)file_stat.st_size / (float)MSS;
        n_matriz = ceil ( aux ) ; //se redondea para arriba (agregar -lm para linkear con libreria de matematica /usr/lib/libm.a)

        /*
        printf("file_stat.st_size: %ld \n", file_stat.st_size);
        printf("MSS: %d \n", MSS);
        printf("n_matriz: %ld \n", n_matriz);
        */

        //envio n_matriz para que el servidor genere su matriz convertimos en char*
        memset( buffer, '\0', TAM );

        sprintf(buffer, "%ld", n_matriz); //guardo n_matriz en buffer para enviar

        n = write( sockfd, buffer, TAM );
        check_socket_write(n);
    }



    //creamos un puntero de punteros (representan las filas en una matriz)

    char **partes_escaneo = malloc( n_matriz * sizeof(char*));
    for(long i = 0; i < n_matriz; i=i+1) partes_escaneo[i] = malloc(MSS * sizeof(char)); //sizeof(char) es 1

    //vamos leyendo la imagen y guardandola en la matriz

    long i=0;

    while( ( bytes_a_guardar = read(fd, partes_escaneo[i], MSS)  ) > 0) { //vamos leyendo archivo

        i = i + 1;
        // printf("bytes_a_guardar %ld, i: %ld \n", bytes_a_guardar, i);

    }

    //envio paquetes
    printf("Enviando escaneo. \n");


    //  sleep(10);



    i = 0;
    while( (n = write(sockfd, partes_escaneo[i], MSS)) > 0 ) { //vamos leyendo archivo

        // printf("%s",partes_escaneo[i]);
        //sleep(0.1);
        //printf("n: %d \n ", n);

        i =i +1 ;
        if(i>= n_matriz) break;
        printf("\rEnviando %ld de %ld paquetes. ",  i+1, n_matriz); //\r para escribir sobre la linea actual


    }




    //liberamos memoria
    for(long i = 0; i < n_matriz; i=i+1) free (partes_escaneo[i]);
    free(partes_escaneo);


    close (fd);

    printf ("\n");

    return 1;

}

/**
@brief Recibe un archivo binario por el mismo socket TPC que es el archivo compilado
con el nuevo numero de version.
Recibimos del firmware el tamaño del archivo para reservar la memoria y luego
procedemos a la descarga. Una vez finalizada la descarga se cierra el socket y
luego se ejecuta el archivo binario reeemplanzado la ejecucion del original.
@param newsockfd descriptor de socket TCP.
@return 0 si hubo un error en ejecutar el firmware.
*/

int recibir_BIN_TCP(int sockfd){


    int n;
    char buffer[TAM];
    long tamano_archivo = 0;
    int fd;


    //confirmamos que estamos listo para recibir archivo
    n = write(sockfd, UPDATE_FIRMWAREBIN, strlen(UPDATE_FIRMWAREBIN));
    check_socket_write(n);

    //esperamos confirmacion
    memset(buffer, '\0', TAM);


    n = read(sockfd, buffer, TAM);
    check_socket_read(n);



    if (strcmp(FILE_NOT_FOUND, buffer) == 0) { //el servidor no puede enviar firmware (caso contarrio recibo tamaño)

        printf("Problemas del servidor para enviar el archivo. \n");
        return 0;
    }


    //leyendo tamaño del archivo

    tamano_archivo = atoi(buffer);

    //Se guarda como firmware.bin en la carpeta recursos_satelite
    remove(FIRMWARE_BIN);
    fd = open(FIRMWARE_BIN, O_RDWR | O_CREAT, 0777); //creamos un archivo firmware bin si no existe con permisos


    memset(buffer, '\0', TAM);

    int n_tot = 0;
    int TAM_AUX;

    if(tamano_archivo < TAM ) TAM_AUX = tamano_archivo;
    else TAM_AUX = TAM;

    printf("Comienza descarga del archivo firmware.bin - Tamaño %ld bytes.	\n", tamano_archivo);

    while( (n = read(sockfd, buffer, TAM_AUX)) ){

      //  if (empieza_con(buffer, FIRM_TRANS_FINISH)) break; //cortamos si termina la transferencia

        n_tot = n_tot + n;

     //   write(fd, buffer, TAM_AUX); //guardo la cantidad n leida en el archivo
        write(fd, buffer, TAM_AUX ); //guardo la cantidad n leida en el archivo

        memset(buffer, '\0', TAM);

        if((tamano_archivo - n_tot) < TAM_AUX )  TAM_AUX = tamano_archivo - n_tot;
        if( (tamano_archivo - n_tot) <= 0 )  break;


    }



    printf("Terminada recepcion del firmware. \n");
    printf("Reiniciando satelite. \n");

    n = write( sockfd, FIN_CONECTION, strlen(FIN_CONECTION) );
    check_socket_write(n);

    close(sockfd); //lo cierra el cliente
    close (fd);

    //ejecuta nuevamente el programa, pisando el proceso actual
    //argv[0] es el nombre del programa y ademas le pasamos los mismos argumentos del programa originalexecv(argv[0], argv);
    char * ls_args[] = { "firmware.bin" , NULL};
    execv(FIRMWARE_BIN, ls_args);

    return 1;
}




/**
@brief Envia la telemetria del satelite: ID, version de software, up time, carga memoria y CPU.
@param sockdf_tcp descriptor de socket TCP.
@param sockaddr_in *cli_addr_udp que contiene la informacion asociada al socket UDP.
@return 1 en caso de envio exitoso, 0 caso contrario
*/

int enviar_telemetria_UDP(int newsockfd_UDP, struct sockaddr_in *dest_addr_udp){

    /*char* ip_addr_dest= inet_ntoa( dest_addr_udp->sin_addr); //convertismos unsignedlog en char formato 192.169...
    int puerto_dest = ntohs(dest_addr_udp->sin_port);

    printf("CLIENTE %s\n", ip_addr_dest );
    printf("PUERTO %d \n", puerto_dest ); */


    int	n;
    socklen_t tamano_direccion;
    tamano_direccion = sizeof( *dest_addr_udp );
    char memoryUsage[TAM];
    char upTIME[TAM];


    double load_CPU = loadCPU();
    char load_CPU_char [sizeof((int)load_CPU)];
    sprintf(load_CPU_char,"%d", (int)load_CPU); //casteamos a int para sacar la coma

    //obtenemos Uso de la memoria (salvo la memoria de SWAP)
    FILE* mem_file = popen("top -n 1 | grep Mem | grep -v \"Swap\" ", "r");
    fgets(memoryUsage, sizeof(memoryUsage),mem_file );
    fclose (mem_file);

    //obtenemos Uso de la memoria (salvo la memoria de SWAP)
    FILE* uptime_file = popen("awk '{print $1}' /proc/uptime", "r");
    fgets(upTIME, sizeof(upTIME),uptime_file );
    fclose (uptime_file);


//	printf("DIFFERENCIA is : %s \n",t_diff_char);

    size_t len1 = strlen("\nID satelite: ");
    size_t len2 = strlen(ID_SATELITE);
    size_t len3 = strlen("\nVersion software: ");
    size_t len4 = strlen(version_software );
    size_t len9 = strlen("Carga procesador (%): ");
    size_t len10 = strlen(load_CPU_char);
    size_t len5 = strlen("\nUp time (segundos): ");
    size_t len6 = strlen(upTIME);
    size_t len7 = strlen("\n");
    size_t len8 = strlen(memoryUsage);

    char str1[len1+len2+len3+len4+len5+len6+len7+ len8+ len9 + len10 +  1];

    //limpiamos buffer
    memset( str1, '\0', sizeof(str1));

//	printf("TAMAÑO DE STR1 is : %ld \n",sizeof(str1));

    strcat (str1,"\nid satelite: ");
    strcat (str1, ID_SATELITE);
    strcat (str1,"\nVersion software: ");
    strcat (str1, version_software  );
    strcat (str1,"\nUp time (segundos): ");
    strcat (str1,upTIME );
    strcat (str1,"Carga procesador (%): ");
    strcat (str1,load_CPU_char);
    strcat (str1,"\n");
    strcat (str1,memoryUsage );
    strcat (str1,"\n");

    printf("Mando telemetria: %s\n", str1);

    n = sendto( newsockfd_UDP, (void *)str1, sizeof(str1), 0, (struct sockaddr *)dest_addr_udp, tamano_direccion );
    if ( n < 0 ) {
        perror( "Escritura en socket" );
        return 0;
    }

    close(newsockfd_UDP);


    return 1;


}



/**
@brief Crea un socket UDP (con la estructura de un servidor UDP) con la misma direccion y puerto que el socket TCP.
Mediante la funcion getsockname extrae la informacion asociada del socket TPC necesaria realizar el bind()
con el nuevo socket UDP ( es necesario debido a que previamente dicho socket TCP se asocia con una direccion IP
y puerto aleatorio mediante la funcion connect()), luego realiza el bind entre el nuevo socket UDP y la
estructura llenada por la funcion getsockname.
@param sockdf_tcp descriptor de socket TCP.
@param sockaddr_in *cli_addr_udp estructura que se carga con la informacion asociada
al socket TPC.
@return nuevo descriptor de socket UDP o 0 en caso de falla en la creacion.
*/

int open_UDP_client(){

    int sockf_udp;

    sockf_udp = socket( AF_INET, SOCK_DGRAM, 0 );
    if (sockf_udp < 0) {
        perror( "apertura de socket" );
        return 0;
    }

    return sockf_udp;

}

/**
@brief Determina si una cadena comieza con otra.
@param str1 cadena a comparar.
@param str2 cadena a comaprar.
@return 1 si cadena str1 comienza con str2, 0 caso contrario.
*/

int empieza_con(char* str1, char* str2){
    if(strncmp(str1, str2, strlen(str2)) == 0) return 1;
    return 0;
}


/**
@brief Obtenemos carga del CPU mediante un promedio de la informacion extraida del /proc/stat.
https://stackoverflow.com/questions/3769405/determining-cpu-utilization
@return carga del CPU, 0 en caso contario.
*/

double loadCPU(void)
{
    long double a[4], b[4], loadavg;
    FILE *fp;

        fp = fopen("/proc/stat", "r");
        fscanf(fp, "%*s %Lf %Lf %Lf %Lf", &a[0], &a[1], &a[2], &a[3]);
        fclose(fp);

        sleep(1);

        fp = fopen("/proc/stat", "r");
        fscanf(fp, "%*s %Lf %Lf %Lf %Lf", &b[0], &b[1], &b[2], &b[3]);
        fclose(fp);

        loadavg = ((b[0] + b[1] + b[2]) - (a[0] + a[1] + a[2])) /
                  ((b[0] + b[1] + b[2] + b[3]) - (a[0] + a[1] + a[2] + a[3]));

    return loadavg*100;

}

