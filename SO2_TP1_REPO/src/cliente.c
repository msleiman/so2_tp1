/**
*	@file cliente.c

*	@brief Ejecuta el lado cliente en un host. Para correrlo se debe ejecutar
* desde la terminal de la forma ./cliente, luego ingresar:
* nombreusuario@ip_servidor:puerto
* El servidor solicitara contraseña, esta es "bigmac".
* Una vez que la autenticacion sea exitosa se podra ejecutar el bash en el
* servidor  y ademas las descargas de archivos mediante el comando:
* descagar nombrearchivo, este archivo se guaradara en el directorio actual
* donde se encuentra el ejecutable cliente.

	@author Sleiman, Mohamad
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <time.h>
#include <math.h>



#include "../includes/cliente_includes.h"
#include "../includes/codigos.h"

#define VERSION_FIRMWARE "./recursos_satelite/n_version.txt"
#define FIRMWARE_BIN "./recursos_satelite/firmware.bin"
#define IMAGEN_SCANNING "./recursos_satelite/imagen.jpg"


time_t start_time;
char *ID_SATELITE = "FCEFYNSAT";
//char *IP_estacion = "192.168.0.10";
char *IP_estacion = "localhost";
char *puerto_estacion = "10301";


int main( int argc, char *argv[] ) {



	int sockfd, puerto, n;
	struct sockaddr_in serv_addr;
	struct hostent *server;
	char buffer[TAM];

	time(&start_time);

    //printf( "Intentado conexion con la estacion - Usuario: %s - IPserver: %s - Puerto %s  \n", ID_SATELITE, IP_estacion, puerto_estacion );


    puerto = atoi( puerto_estacion );
	sockfd = socket( AF_INET, SOCK_STREAM, 0 );
	if ( sockfd < 0 ) {
		perror( "ERROR apertura de socket" );
		exit( 1 );
	}

	/*
	* gethostbyname devuelve una estructura del tipo hostent
	* para una direccion de host dada.
	* si le pasamos como parametro una direccion IPv4
	* copia el nombre en el campo h_name ( copia la direccion
	* como un char), h_addrtype en 2 para AF_INET,
	* h_length en 4 para IPv4 y
	* su estructura in_addr en el campo h_addr_list
	*
	* la estructura hostent sirve para asociar los datos
	* del cliente.
	*/

	server = gethostbyname( IP_estacion );
	if (server == NULL) {
		fprintf( stderr,"Error, no existe el host\n" );
		exit( 1 );
	}


	memset( (char *) &serv_addr, '0', sizeof(serv_addr) );
	serv_addr.sin_family = AF_INET;

	//copiamos el contenido de la struct in_addr en la struct sockaddr_in del server
	bcopy( (char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length );
	serv_addr.sin_port = htons( puerto );

	/*
	* Funcion connect establece la conexion con el server
	* Si el socket no se ha vinculado a una dirección local,
	* connect () lo vinculará a una.
	*/

	int con_status = -1;

	while (1){ //esperamos hasta lograr la conexion

		printf( "Intentado conexion con la estacion - Usuario: %s - IPserver: %s - Puerto %s  \n", ID_SATELITE, IP_estacion, puerto_estacion );

		con_status =  connect( sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr ) );
		// printf ("conecct: %d \n ", auxux );

		if ( con_status  >= 0 )
		{
			printf( "Conexion exitosa.  \n");
			break;
		}

		sleep(2); //si no logramos la conexion dormimos 10 segundos

	}


	  //MENSAJE DE BIENVENIDA ACA PODRIA MANDAR NUMERO DE SATELITE
       memset( buffer, 0, TAM );
       memcpy(buffer, "Satelite conectado. \n", 23);

  	  n = write( sockfd, buffer, strlen(buffer) );
  	  check_socket_write(n);


    while(1) {

        memset( buffer, '\0', TAM );

        n = read( sockfd, buffer, TAM );
        check_socket_read(n);

		printf( "Recibí de la estacion: %s", buffer );

		buffer[strlen(buffer) - 1] = '\0'; //agregamos fin de archivo

		if( !strcmp( "exit", buffer ) ) {

			n = write( sockfd, FIN_CONECTION, strlen(FIN_CONECTION));
            check_socket_write(n);

			exit(1);
			break;

		} else if ( strcmp(buffer, OBTENER_TELEMETRIA ) == 0 ) {

			//Abrimos el socket UDP y la ligamos a la structura sockaddr

			int newsockfd_UDP = open_UDP_client();

			//confirmamos apertura del puerto UDP
			n = write(sockfd, OBTENER_TELEMETRIA, 18);
			check_socket_write(n);

				    //Esperamos la confirmacion de la apertura de puerto UDP del server
				    memset(buffer, '\0', TAM);

				    n = read(sockfd, buffer, TAM);
			check_socket_read(n);

				    if (strcmp(UDP_OPEN_OK, buffer) == 0) {
					    enviar_telemetria_UDP(newsockfd_UDP, &serv_addr);
				    } else {
					    printf("Problemas para abrir el socket UDP en el server. \n");
				    }

				    close(newsockfd_UDP);


			    } else if ( strcmp(buffer, UPDATE_FIRMWAREBIN) == 0 ){

			printf("Preparando recepcion del firmware. \n");
			recibir_BIN_TCP(sockfd);
			printf("Terminada recepcion del firmware. \n");
			printf("Reiniciado satelite. \n");

			n = write( sockfd, FIN_CONECTION, strlen(FIN_CONECTION) );
			check_socket_write(n);

			close(sockfd);

			//ejecuta nuevamente el programa, pisando el proceso actual
			//argv[0] es el nombre del programa y ademas le pasamos los mismos argumentos del programa original
			execv(argv[0], argv);



		} else if ( strcmp(buffer, START_SCANNING) == 0 ){

			printf("Solicitud de escaneo. \n");
			scanning(sockfd);
			printf("Terminado el escaneo. \n");


		} else {

			n = write( sockfd, "Comando no identificado \n", 26 );
		    check_socket_write(n);
		}


    }

	return 0;

}


int scanning (int sockfd){

    int n;
    long n_matriz = 0;
    char buffer[TAM];
    int fd;
    struct stat file_stat;
    long bytes_a_guardar;


    //confirmamos inicio de escaneo
    n = write(sockfd, START_SCANNING, strlen(START_SCANNING));
    check_socket_write(n);

    //abrimos archivo
    fd = open(IMAGEN_SCANNING, O_RDONLY);

    //aca pedir el mms en una funcion

    //abrimos imagen


    if( (fd == -1) || fstat(fd, &file_stat)   ) { //error abriendo el archivo o leyendo datos

        n = write( sockfd, FILE_NOT_FOUND, strlen(FILE_NOT_FOUND) );
        check_socket_write(n);

        return 0;

    } else {

        n = write( sockfd, FILE_FOUND, strlen(FILE_FOUND) );
        check_socket_write(n);

    }


    //return 0;

    //proceso de division de la imagen, a la imagen la divido en matrices:
    //calculo el tamaño de la matriz nxm (m es el MSS)

    float aux =  (float)file_stat.st_size / (float)MSS;
    n_matriz = ceil ( aux ) ; //se redondea para arriba (agregar -lm para linkear con libreria de matematica /usr/lib/libm.a)

    /*
    printf("file_stat.st_size: %ld \n", file_stat.st_size);
    printf("MSS: %d \n", MSS);
    printf("n_matriz: %ld \n", n_matriz);
    */

    //creamos un puntero de punteros (representan las filas en una matriz)

    char **partes_escaneo = malloc( n_matriz * sizeof(char*));
    for(long i = 0; i < n_matriz; i=i+1) partes_escaneo[i] = malloc(MSS * sizeof(char)); //sizeof(char) es 1

    //vamos leyendo la imagen y guardandola en la matriz

    long i=0;

    while( ( bytes_a_guardar = read(fd, partes_escaneo[i], MSS)  ) > 0) { //vamos leyendo archivo

            i = i + 1;
            // printf("bytes_a_guardar %ld, i: %ld \n", bytes_a_guardar, i);

    }



    //envio n_matriz para que el servidor genere su matriz convertimos en char*
    memset( buffer, '\0', TAM );

    sprintf(buffer, "%ld", n_matriz); //guardo n_matriz en buffer para enviar

    n = write( sockfd, buffer, strlen(buffer) );
    check_socket_write(n);

    //envio paquetes
    printf("Enviando escaneo. \n");

    sleep(10);

    i = 0;
   while( write(sockfd, partes_escaneo[i], MSS) > 0 ) { //vamos leyendo archivo

        i =i +1 ;
        if(i>= n_matriz) break;
        printf("\rEnviando %ld %% de %ld paquetes. ",  i+1, n_matriz); //\r para escribir sobre la linea actual


   }


    //liberamos memoria
    for(long i = 0; i < n_matriz; i=i+1) free (partes_escaneo[i]);
    free(partes_escaneo);

    close (fd);

    printf ("\n");

    return 1;

}

int recibir_BIN_TCP(int sockfd){


    int n;
    char buffer[TAM];
    long tamano_archivo = 0;
//    long tamano_restante = 0 ;
    int fd;


    //confirmamos que estamos listo para recibir archivo
    n = write(sockfd, UPDATE_FIRMWAREBIN, strlen(UPDATE_FIRMWAREBIN));
    check_socket_write(n);

    //esperamos confirmacion
    memset(buffer, '\0', TAM);


    n = read(sockfd, buffer, TAM);
    check_socket_read(n);
    
 
 
    if (strcmp(FILE_NOT_FOUND, buffer) == 0) { //el servidor no puede enviar firmware (caso contarrio recibo tamaño)

        printf("Problemas del servidor para enviar el archivo. \n");
        return 0;
    } 
        
    
    //leyendo tamaño del archivo

    tamano_archivo = atoi(buffer);

    printf("Comienza descarga del archivo firmware.bin - Tamaño %ld bytes.	\n", tamano_archivo);

    //Se guarda como firmware.bin en la carpeta recursos_satelite
    fd = open(FIRMWARE_BIN, O_RDWR | O_CREAT, 0666); //creamos un archivo firmware bin si no existe

//    tamano_restante = tamano_archivo;

    

    
  
    memset(buffer, '\0', TAM);


    while( (n = read(sockfd, buffer, TAM)) ){


	if (empieza_con(buffer, FIRM_TRANS_FINISH)) break; //cortamos si termina la transferencia
	  
	write(fd, buffer, n); //guardo la cantidad n leida en el archivo
	
        memset(buffer, '\0', TAM);


	}
    

	//Abrimos archivo de firmware y guardamos contenido.
	char *version_software = load_file(FIRMWARE_BIN);
	//printf ("version software %s", version_software);
	//copiamos numero de version en el archivo v_software.txt
	write_file (VERSION_FIRMWARE, version_software);


	free(version_software);
	close (fd);
	
	
//	printf("\n");
	return 1;

}




/**
@brief Recibe el archivo descargado a traves del socket UDP.
Crea un archivo nuevo en el directorio actual cuyo nombre es igual al archivo a descagar,
recibe el tamaño total del archivo y luego permanece en un bluce recibiendo datos por el socket
y escribiendolos en el archivo nuevo, hasta recibir un UDP_TRANS_OK que indica el fin de la transferencia.
En medio de la recepcion de datos muestra y actualiza el porcentaje de descaga actual del archivo. En en caso
de que la descarga no se complete al 100% se muestra un anuncio en el stdout.
@param newsockfd_UDP descriptor de socket TCP.
@param sockaddr_in *cli_addr_udp estructura con las direcciones y puerto del cliente asociadas al socket UDP.
@param nombre_archivo nombre del archivo creado igual al nombre del archivo a descargar.
@return 1.
*/

int enviar_telemetria_UDP(int newsockfd_UDP, struct sockaddr_in *dest_addr_udp){

	/*char* ip_addr_dest= inet_ntoa( dest_addr_udp->sin_addr); //convertismos unsignedlog en char formato 192.169...
	int puerto_dest = ntohs(dest_addr_udp->sin_port);

	printf("CLIENTE %s\n", ip_addr_dest );
	printf("PUERTO %d \n", puerto_dest ); */


	int	n;
	socklen_t tamano_direccion;
	tamano_direccion = sizeof( *dest_addr_udp );
	char memoryUsage[TAM];



	//leemos el archivo version de firmware y extraemo el contenido!
	char *version_software = load_file(VERSION_FIRMWARE);


	time_t now_time;
	time(&now_time);

	double t_diff = difftime (now_time, start_time);
	char t_diff_char [sizeof((int)t_diff)];
	sprintf(t_diff_char,"%d", (int)t_diff); //casteamos a int para sacar la coma

	//obtenemos Uso de la memoria (salvo la memoria de SWAP)
	FILE* file = popen("top -n 1 | grep Mem | grep -v \"Swap\" ", "r");
	fgets(memoryUsage, sizeof(memoryUsage),file );
	fclose (file);


//	printf("DIFFERENCIA is : %s \n",t_diff_char);

	size_t len1 = strlen("\nID satelite: ");
	size_t len2 = strlen(ID_SATELITE);
	size_t len3 = strlen("\nVersion software: ");
	size_t len4 = strlen(version_software );
	size_t len5 = strlen("Up time (segundos): ");
	size_t len6 = strlen(t_diff_char);
	size_t len7 = strlen("\n");
	size_t len8 = strlen(memoryUsage);

	char str1[len1+len2+len3+len4+len5+len6+len7+len8+1];

	//limpiamos buffer
	memset( str1, '\0', sizeof(str1));

//	printf("TAMAÑO DE STR1 is : %ld \n",sizeof(str1));

	strcat (str1,"\nid satelite: ");
	strcat (str1, ID_SATELITE);
	strcat (str1,"\nVersion software: ");
	strcat (str1, version_software  );
	strcat (str1,"Up time (segundos): ");
	strcat (str1,t_diff_char );
	strcat (str1,"\n");
	strcat (str1,memoryUsage );
	strcat (str1,"\n");

	printf("Mando telemetria: %s\n", str1);

	n = sendto( newsockfd_UDP, (void *)str1, sizeof(str1), 0, (struct sockaddr *)dest_addr_udp, tamano_direccion );
	if ( n < 0 ) {
		perror( "Escritura en socket" );
		return 0;
	}

	close(newsockfd_UDP);
	free (version_software);


	return 1;


}



/**
@brief Crea un socket UDP (con la estructura de un servidor UDP) con la misma direccion y puerto que el socket TCP.
Mediante la funcion getsockname extrae la informacion asociada del socket TPC necesaria realizar el bind()
con el nuevo socket UDP ( es necesario debido a que previamente dicho socket TCP se asocia con una direccion IP
y puerto aleatorio mediante la funcion connect()), luego realiza el bind entre el nuevo socket UDP y la
estructura llenada por la funcion getsockname.
@param sockdf_tcp descriptor de socket TCP.
@param sockaddr_in *cli_addr_udp estructura que se carga con la informacion asociada
al socket TPC.
@return nuevo descriptor de socket UDP o 0 en caso de falla en la creacion.
*/


int open_UDP_client(){

	int sockf_udp;

	sockf_udp = socket( AF_INET, SOCK_DGRAM, 0 );
	if (sockf_udp < 0) {
		perror( "apertura de socket" );
		return 0;
	}

	return sockf_udp;

}

/**
@brief Determina si una cadena comieza con otra.
@param str1 cadena a comparar.
@param str2 cadena a comaprar.
@return 1 si cadena str1 comienza con str2, 0 caso contrario.
*/

int empieza_con(char* str1, char* str2){
	if(strncmp(str1, str2, strlen(str2)) == 0) return 1;
	return 0;
}


//cargamos el archivo en el buffer
//https://stackoverflow.com/questions/3747086/reading-the-whole-text-file-into-a-char-array-in-c
char* load_file(char const* path)
{
    char* buffer = 0;
    long length;
    FILE * f = fopen (path, "rb"); //was "rb"

    if (f != NULL)
    {
        fseek (f, 0, SEEK_END);
        length = ftell (f);
        fseek (f, 0, SEEK_SET);
        buffer = (char*)malloc ((length+1)*sizeof(char));
        if (buffer)
        {
            fread (buffer, sizeof(char), length, f);
        }
        fclose (f);
    }
    buffer[length+1] = '\0';

    return buffer;
}

void write_file(char const* path, char* buffer){


	//printf ("abriendo %s", path);

	FILE * f = fopen (path, "w"); //was "rb"

	if (f != NULL){
		//printf ("abierto ");
		fprintf(f, "%s", buffer);

	}

    fclose(f);

}
