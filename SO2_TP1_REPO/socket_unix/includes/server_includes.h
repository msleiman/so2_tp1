/** @file server_includes.c

    @brief contiene la declaracion de funcion y variables utlizadas en el servidor.

 */

#define TAM 1448
#define MSS 1448 // 1500 MTU - 20 BYTE TCP HEADER - 20 BYTES IP HEADER = 1460
#define MAX_CONEXIONES 5

int autenticacion();
int empiezaCon(char* , char* );
int recibir_telemetria_UDP(int , struct sockaddr_un *);
int enviar_BIN_TCP(int);
int open_UDP_server(int sockfd_tcp, struct sockaddr_un *);
int recieve_scanning(int,time_t);
void vaciar_buffer_teclado();