/**
*	@file servidor.c

*	@brief Ejecuta el lado servidor en un host. Para correrlo se debe ejecutar
* desde la terminal de la forma ./serivdor direccionIPv4 puerto.
* La contraseña del servidor es "bigmac".
* Acepta multiples conexiones simultaneas, permite la ejecucion de un programa
* bash y envia la salida hacia el cliente. Permita tambien la desarga de archivos
* hacia el cliente.

	@author Sleiman, Mohamad
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>

//DEFINIR UN BUFFER PIPE MUY GRANDE Y UN END MENSAJE PARA TCP PARA MANDAR COSAS GRANDES.
#include "../includes/server_includes.h"
#include "../includes/codigos.h"

/**
@brief Funcion main del cliente.
Recibe direccion y puerto del servidor como parametros en arvg[] y abre el puerto TCP
que queda a la espera de conexiones de clientes; acepta hasta 5 conexiones de
clientes simulataneas. Cuando recibe una solicitud de conexion del socket implementa
la funcion de autenticacion. Procesa los comandos recibidos por el socket TCP desde el cliente que
pueden realizar acciones sobre el bash, descargar archivos o cerar conexion cuando se
recibe la cadena exit. Cuando el comando recibido desde el cliente es del tipo
descarga nombresdearchivo, el servidor envia una confirmacion al mismo indicado la existencia o no
del archivo, en caso de existir queda a la espera de la confirmacion de creacion del puerto UDP
del lado cliente y luego crea su socket para la transferencia del arhivo.
*/



#define FIRMWARE_BIN "./recursos_estacion/firmware.bin"
#define IMAGEN_SCANNING "./recursos_estacion/imagen.jpg"


int main(int argc, char *argv[]) {

   /* char cwd[2048];
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        printf("Current working dir: %s\n", cwd);
    } else {
        perror("getcwd() error");

    } */


   //////// AUTENTICACION ////////
	/*int autenticacion_ok = autenticacion();
	int autenticacion_ok = 1;
	if (autenticacion_ok == 0) exit(0); //nos vamos del programa
	*/


    //////// CREACION DE SOCKET ////////

	int sockfd, newsockfd, puerto;
	pid_t pid;
	socklen_t clilen;
	char buffer[TAM];
	char *direccionIPv4;
	struct sockaddr_in serv_addr, cli_addr;
	int n;

    if (argc < 3) { //comprobamos funcionamiento en llamada
		fprintf( stderr, "Uso: %s <ipv4 puerto>\n", argv[0]);
		exit(1);
	}


	/*Creacion de socket
	* int socket( int domain, int type, int protocol );
	* domain: tipo de direccion almacenada (AF_INET dominio de internet)
	* tipo: indica tipo de socket (sock_stream secuencia de caracateres)
	* protocolo: 0, significa que el protocolo es un valor por defecto
	* determinado por domain y type (que en este caso da un socket TCP).
	*
	* Devuelve un file descriptor con el numero mas bajo que no este
	* abierto para el proceso
	*/
	sockfd = socket( AF_INET, SOCK_STREAM, 0); //socket le dice al SO que queremos un descriptor de archivo de socket.
	if (sockfd < 0) {
		perror(" ERROR en la apertura de socket ");
		exit(1);
	}

	direccionIPv4 = argv[1]; //asignamos una ip (De la interfaz interfaz ifconfig  --> 192.168....)
	puerto = atoi(argv[2]);

	//limpieza de estructura
	memset((char *) &serv_addr, 0, sizeof(serv_addr));

	//completamos la estructura
	serv_addr.sin_family = AF_INET;
	inet_aton(direccionIPv4, &serv_addr.sin_addr); //funcion que transforma un direccion Ipv4 en binario.
	serv_addr.sin_port = htons(puerto); //htons convierte el INT UNSIGNED del orden de bytes del host a orden de bytes de la red (que es BIG ENDIAN)

	/*
	* hacemos el bind, primero casteamos la struc sockaddr_in a sockaddr
	 * (ligamos puerto y direccion IP a un socket)
	*/

	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) { //PREGUNTAR
		perror("ligadura");
		exit(1);
	}

	printf("Proceso: %d - socket disponible --->  IPv4 %s - puerto %d \n",
	getpid(), direccionIPv4, ntohs(serv_addr.sin_port));

	/*
	* listen pone en escucha un socket, que acepta MAX_CONEXIONES conexiones simultaneas
	*
	* accept se encarga de aceptar una conexion entrante en un socket y crea
	* otro socket inmediatamente con las mismas propiedades,
	* el socket original vuelve al estado de escucha.
	*/


	listen(sockfd, MAX_CONEXIONES); //sockdf es un socket pasivo para aceptar solicitudes de conexiones
	clilen = sizeof(cli_addr);

	while (1) {

	    /*
	     * accept() extrae la primer solicitud de conexion en la cola de conexiones pendientes
	     * para el socket de escucha (sockfd), crea un nuevo socket y devuelve el file descriptor.
	     * Este nuevo socket esta formado por las direcciones y puertos locales y remotos.
	     */

		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen); //
		if (newsockfd < 0) {
			perror("accept");
			exit(1);
		}


		pid = fork(); //Rutina para el cliente
		if (pid < 0) {
			perror("fork");
			exit(1);
		}

		if (pid == 0) {  // Proceso hijo, cerramos el file descritpor sockfd del hijo (no en el padre)

			close(sockfd);

			n = read( newsockfd, buffer, TAM-1 );
            check_socket_read(newsockfd);



            while ( 1 ) {

				memset(buffer, '\0', TAM);

				printf("enviar a estacion >> ");
				fgets(buffer, TAM - 1, stdin);

				n = write(newsockfd, buffer, sizeof(buffer));
				check_socket_write(n);


				//leo respuesta de la estacion
                memset( buffer, '\0', TAM );

                n = read( newsockfd, buffer, TAM-1 );
                check_socket_read(n);


                if( !strcmp(buffer, FIN_CONECTION ) ) {

                    printf("PROCESO %d. Satelite desconectado. \n", getpid());
                    exit(1);

                } else if ( strcmp(OBTENER_TELEMETRIA, buffer) == 0 ){ //cliente me confirma apertura de puerto

                    printf("PROCESO %d. Recepcion de telemetria. \n", getpid());

                    //Abro puerto UDP
					struct sockaddr_in srv_addr_udp;
					int newsock_udp = open_UDP_server(newsockfd, &srv_addr_udp);


					if( newsock_udp == 0){

                        printf("PROCESO %d. Problemas en la apertura del socket UDP. \n", getpid());

                        n = write(newsockfd, MS_FAIL_AUTH, strlen(MS_FAIL_AUTH));
						check_socket_write(n);

					} else {

					    //Confirmo apertura de mi puerto UDP sin problemas
						n = write(newsockfd, UDP_OPEN_OK, strlen(UDP_OPEN_OK));
						check_socket_write(n);

						//Recibo descarga UDP
						recibir_telemetria_UDP(newsock_udp, &srv_addr_udp);
						close(newsock_udp);

					}


                } else if ( strcmp( buffer, UPDATE_FIRMWAREBIN) == 0 ){

                    printf("PROCESO %d. Preparando envio del firmware. \n", getpid());
                    enviar_BIN_TCP(newsockfd);
                    printf("PROCESO %d. Finalizado envio del firmware. \n", getpid());


                    //recibo mensaje de fin para cerrar la conexion

                    memset( buffer, '\0', TAM );

                    n = read( newsockfd, buffer, TAM-1 );
                    check_socket_read(n);


                    if ( strcmp(FIN_CONECTION, buffer) == 0 ) {

                        printf("PROCESO %d. Satelite reiniciadose. \n", getpid());
                        printf("PROCESO %d. Conexion cerrada. \n", getpid());
                        exit(1);


                    }

                } else if ( strcmp(buffer, START_SCANNING) == 0 ){

					printf("PROCESO %d. Activo rutina de escaneo en el satelite. \n", getpid());
					recieve_scanning(newsockfd);
					printf("PROCESO %d. Finalizado escaneo del satelite. \n", getpid());

                } else {

                	printf("Recibi del satelite: %s", buffer);
                }

		}

		} else {
			printf("SERVIDOR: Nuevo cliente, que atiende el proceso hijo: %d\n", pid);
			close(newsockfd); //proceso padre cerramos el file descriptor newsockfd en el padre
		}
	}
}


int recieve_scanning(int newsockfd){

    int n;
    char buffer[TAM];
    long n_matriz = 0;
    int fd;

    memset(buffer, '\0', TAM);

    n = read(newsockfd, buffer, TAM);
    check_socket_read(n);

    if (strcmp(FILE_NOT_FOUND, buffer) == 0) { //el servidor no puede realizar el escaneo

        printf("Problemas del satelite para realizar el escaneo. \n");
        return 0;
    }

   // return  0;

    //recibo cantidad de paquetes para allocar la matriz

    n = read(newsockfd, buffer, TAM);
    check_socket_read(n);

    n_matriz = atoi(buffer);

    printf("PROCESO %d. Numero de paquetes %ld \n", getpid(), n_matriz);


    // printf (" N_MATRIZ: %ld \n", n_matriz);

    char **partes_escaneo = malloc( n_matriz * sizeof(char*));
    for(long i = 0; i < n_matriz; i=i+1) partes_escaneo[i] = malloc(MSS * sizeof(char)); //sizeof(char) es 1

    //recibimos paquetes
    printf("PROCESO %d. Recibiendo escaneo. \n", getpid());

    sleep(1);

    long i = 0;

    while( read(newsockfd, partes_escaneo[i], MSS) > 0 ) { //vamos leyendo archivo


        i =i +1 ;
        if(i >= n_matriz) break;
        printf("\rDescargando %ld %% de %ld paquetes. ",  i+1, n_matriz); //\r para escribir sobre la linea actual



    }


    /*
    for(long i = 0; i < n_matriz; i=i+1){


        recv(newsockfd, partes_escaneo[i], MSS, 0);

        // n = read( newsockfd, partes_escaneo[i], MSS );
        //check_socket_read(n);

        //printf("%s ", partes_escaneo[i];

        printf("\rDescargando %ld %% de %ld paquetes. ",  i+1, n_matriz); //\r para escribir sobre la linea actual


    } */


    printf("\n");
    printf("PROCESO %d. Rearmando escaneo. \n", getpid());

    //creamos el archivo imagen.jpg en carpeta recursos_estacion
    fd = open(IMAGEN_SCANNING, O_RDWR | O_CREAT, 0666); //creamos un archivo firmware bin si no existe

    for(long i = 0; i < n_matriz; i=i+1){

        n = write( fd, partes_escaneo[i], MSS );

    }


    for(long i = 0; i < n_matriz; i=i+1) free (partes_escaneo[i]);
    free(partes_escaneo);
    close (fd);

	return 1;

}


int enviar_BIN_TCP(int newsockfd){


    int fd;
    int	n;
    char buffer[TAM];
    struct stat file_stat; //para obtener informacion del archivo
    int bytes_a_enviar = 0;

    memset(buffer, '\0', TAM);

    fd = open(FIRMWARE_BIN, O_RDONLY);


    if( (fd == -1) || (fstat(fd, &file_stat) < 0)   ) { //error abriendo el archivo o leyendo datos

        n = write( newsockfd, FILE_NOT_FOUND, strlen(FILE_NOT_FOUND) );
		check_socket_write(n);

        return 0;

    } else {    //envio tamaño del archivo

        sprintf(buffer, "%ld", file_stat.st_size);

        printf("PROCESO %d. Tamaño del firmware a enviar: %ld bytes. \n", getpid(), file_stat.st_size);

        n = write(newsockfd, buffer, sizeof(buffer));
        check_socket_write(n);

    }


    printf("PROCESO %d. Enviando archivo. \n", getpid());



    while( ( bytes_a_enviar = read(fd, buffer, TAM) ) > 0 ) { //vamos leyendo archivo y enviando


		n = write(newsockfd, buffer, sizeof(buffer));
		check_socket_write(n);

    }

    n = write(newsockfd, FIRM_TRANS_FINISH, strlen(FIRM_TRANS_FINISH));
	check_socket_write(n);



    close (fd);
    return 1;

}








/**
@brief Sincroniza la creacion del socket UDP con el cliente, mediante el socket TCP.
Queda a la espera en el socket TPC de la confirmacion desde el cliente de la creacion
del socket UDP, esta confirmacion se realiza mediante el mensaje UDP_OPEN_OK. En caso de el
cliente tenga problemas para abrir el socket UDP la cadena de caracateres recibida sera distinta
al mensaje de confirmacion. Una vez recibida la confirmacion abrimos nuestro socket UDP.
@param newsockfd descriptor de socket TCP.
@return 1 si se creo de forma exitosa el socket UDP en el cliente, 0 caso contrario.
*/



int open_UDP_server(int sockfd_tcp, struct sockaddr_in *srv_addr_udp)
{
	int sockfd_udp;

	/*getsocketname recupera los datos de la struct sockaddr_in ligada al server*/
	struct sockaddr_in srv_addr_tcp;
	socklen_t length = sizeof(struct sockaddr_in);
	getsockname(sockfd_tcp, (struct sockaddr *) &srv_addr_tcp, &length);

	char *ip_addr_server = inet_ntoa( srv_addr_tcp.sin_addr);
    int puerto = ntohs(srv_addr_tcp.sin_port);

	printf("IP_SERVER %s\n" ,ip_addr_server ) ;
	printf("puerto %d\n", puerto);


	sockfd_udp = socket( AF_INET, SOCK_DGRAM, 0 );
	if (sockfd_udp < 0) {
		perror("ERROR en apertura de socket");
		return 0;
	}

	//ya es una direccion le sacamos los & y agreagamos *
	memset( srv_addr_udp, 0, sizeof(*srv_addr_udp) );
	srv_addr_udp->sin_family = AF_INET;
	inet_aton(ip_addr_server, &srv_addr_udp->sin_addr);
	srv_addr_udp->sin_port = htons( puerto );
	memset( (srv_addr_udp->sin_zero), '\0', 8 );

	if( bind( sockfd_udp, (struct sockaddr *) srv_addr_udp, sizeof(*srv_addr_udp) ) < 0 ) {
        perror( "ERROR en binding" );
		return 0;
	}

	printf( "Socket UDP disponible en puerto: %d\n", ntohs(srv_addr_udp->sin_port) );

	return sockfd_udp;
}

int recibir_telemetria_UDP(int newsockfd_UDP, struct sockaddr_in *srv_addr_udp){

	char buffer[ TAM ];
	memset( buffer, 0, TAM );
	socklen_t tam_direccion = sizeof( struct sockaddr );

	recvfrom( newsockfd_UDP, buffer, TAM, 0, (struct sockaddr *)srv_addr_udp, &tam_direccion );

	printf("Recibo telemetria: \n %s	\n", buffer);

	return 1;


}



/**
@brief Determina si una cadena comieza con otra.
@param str1 cadena a comparar.
@param str2 cadena a comaprar.
@return 1 si cadena str1 comienza con str2, 0 caso contrario.
*/

int empiezaCon(char* str1, char* str2){

	if(strncmp(str1, str2, strlen(str2)) == 0) return 1;
	return 0;

}

/**
@brief Funcion que realiza la autenticacion del cliente. Cuando es ejectuada esta
funcion debe recibir por primera y unica vez el nombre de usuario que intenta
autenticarse, luego permanece en un bucle recibiendo cadenas de caracateres
que es enviada desde el cliente, si la cadena de caracteres es correcta,
el sevidor envia un mensaje MSG_WELCOME indicando que la autenticacion es exitosa.
En caso contrario la operacion se reitera una cantidad de veces determinada por
una variable de control hasta se ingrese correctamente la contraseña o se acabe la
cantidad de reintentos, ocurre esto ultimo se envia un mensaje MS_FAIL_AUTH.
@param sockdf_tcp descriptor de socket TCP.
@return 1 autenticacion exitosa, 0 caso contrario.
*/

int autenticacion() {

	char buffer[TAM];
	int control = 1;
	int exito = 2;
	int reintentos = 3;


	while(1){

		memset( buffer, '\0', TAM );

		printf("Ingrese su contraseña: " );
		scanf("%s", buffer);
		buffer[strlen(buffer)] = '\0'; //Indicamos la terminacion del string.

		if (strcmp("bigmac", buffer) == 0) {

		    printf(	"Autenticacion exitosa. \n");
	    	exito = 1;

        } else if(control >= reintentos){

			printf(	"Autenticacion fallida. \n");
	        exito = 0;

        } else {

            control = control + 1;
			printf("Volver a intentar. \n" ); //aca recibe usuario

		}

		if(exito == 1 || exito == 0) break;


	}

	return exito;

}
